var xClientKey = '15da56db54707843993d8d9bd37a21fb'; // HEST
var xDataFormat = 'json';

var run_env = 'prod';

switch (run_env) {
    case 'test':
        var api_location = ($.browser.msie ? "https://ec2013.gc-webtv.dk/api/" : "https://wtvapi1.gc-webtv.dk/");
        var showConsoleMsg = true;
        break;
    case 'prod':
        var api_location = ($.browser.msie ? "https://ec2013.gc-webtv.dk/api/" : "https://wtvapi1.gc-webtv.dk/");
        var showConsoleMsg = false;
        break;
    default:
        alert('"run_env" not valid');
}

function consoleLog(msg) {
    if (!$.browser.msie && showConsoleMsg) {
        console.log(msg);
    }
}


angular.module('categoryModule', ['ngResource']).
    config(function($httpProvider){
        $httpProvider.defaults.headers.common['X-ClientKey'] = xClientKey;
        $httpProvider.defaults.headers.common['X-DataFormat'] = xDataFormat;
        $httpProvider.defaults.useXDomain = true;
     }).
    factory('Categories', function ($resource, $rootScope, $http) {
        var Categories = $resource(api_location + 'categories'
            );
        
        return Categories;
    });

angular.module('videosModule', ['ngResource']).
    config(function($httpProvider){
        $httpProvider.defaults.headers.common['X-ClientKey'] = xClientKey;
        $httpProvider.defaults.headers.common['X-DataFormat'] = xDataFormat;
        $httpProvider.defaults.useXDomain = true;
    }).
    factory('Videos', function ($resource, $rootScope) {
        var Videos = $resource(api_location + 'video/:id' + '?offset=:theOffset&size=:theSize' ,
                {id: "5", theOffset: "0", theSize: "1000"},
                {update: {method: 'PUT'}}
            );

        return Videos;
    });    

angular.module('metadataModule', ['ngResource']).
    config(function($httpProvider){
        $httpProvider.defaults.headers.common['X-ClientKey'] = xClientKey;
        $httpProvider.defaults.headers.common['X-DataFormat'] = xDataFormat;
        $httpProvider.defaults.useXDomain = true;
     }).
    factory('MetaD', function ($resource, $rootScope) {
        var MetaD = $resource(api_location + 'metadata/:id' ,
                {id: "76"}
            );
        return MetaD;
    });


angular.module('userModule', ['ngResource']).
    config(function($httpProvider){
        $httpProvider.defaults.headers.common['X-ClientKey'] = xClientKey;
        $httpProvider.defaults.headers.common['X-DataFormat'] = xDataFormat;
        $httpProvider.defaults.useXDomain = true;
   }).
    factory('User', function ($resource, $rootScope) {
        var User = $resource(api_location+'endusers/:id' ,
                {},
                {save: {method: 'PUT'}}
            );

        return User;
    });

angular.module('shopModule', ['ngResource']).
    config(function($httpProvider){
        $httpProvider.defaults.headers.common['X-ClientKey'] = xClientKey;
        $httpProvider.defaults.headers.common['X-DataFormat'] = xDataFormat;
        $httpProvider.defaults.useXDomain = true;
   }).
    factory('ShopItems', function ($resource, $rootScope) {
        var ShopItems = $resource(api_location+'shop/items/:id' ,
                {}
            );

        return ShopItems;
    });
