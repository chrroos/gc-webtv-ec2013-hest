/* Use this script if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'generic\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon-generic-help' : '&#xe000;',
			'icon-generic-warning' : '&#xe001;',
			'icon-generic-info' : '&#xe002;',
			'icon-generic-cross' : '&#xe003;',
			'icon-generic-checkmark' : '&#xe004;',
			'icon-generic-reply' : '&#xe005;',
			'icon-generic-reply-2' : '&#xe006;',
			'icon-generic-undo' : '&#xe007;',
			'icon-generic-forward' : '&#xe008;',
			'icon-generic-star' : '&#xe009;',
			'icon-generic-text' : '&#xe00a;',
			'icon-generic-text-2' : '&#xe00b;',
			'icon-generic-arrow-down' : '&#xe00c;',
			'icon-generic-list' : '&#xe00d;',
			'icon-generic-play' : '&#xe00e;',
			'icon-generic-pause' : '&#xe00f;',
			'icon-generic-record' : '&#xe010;',
			'icon-generic-stop' : '&#xe011;',
			'icon-generic-next' : '&#xe012;',
			'icon-generic-previous' : '&#xe013;',
			'icon-generic-first' : '&#xe014;',
			'icon-generic-last' : '&#xe015;',
			'icon-generic-menu' : '&#xe016;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; i < els.length; i += 1) {
		el = els[i];
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-generic-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};