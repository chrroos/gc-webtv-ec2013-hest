var default_stream_res = 'wtv';

// ['profile_720', 'profile_1080', 'profile_1080h', 'profile_android', 'profile_ios', 'profile_wtv'];
var valid_od_streamtypes = [
    {
        profile_name: 'no_valid',
        display_name: 'Will not be displayed, because profile name does no exist!!!',
        hideOnMobile: false
    },
    {
        profile_name: 'profile_android',
        display_name: 'Low',
        hideOnMobile: false
    },
    {
        profile_name: 'profile_wtv',
        display_name: 'Web',
        hideOnMobile: false
    },
    { 
        profile_name: 'profile_720',
        display_name: 'HD (720p)',
        hideOnMobile: false
    }, 
    { 
        profile_name: 'profile_1080',
        display_name: 'HD (1080p)',
        hideOnMobile: true
    }, 
    {
        profile_name: 'profile_1080h',
        display_name: 'HD HQ (1080p)',
        hideOnMobile: true
    }
];


var validation = {
    age_from: 12, // Years user must be older than
    age_to: 100, // Years user must be younger than! ()
    cookieExpAfterDays: 7
};


var custom_strings = {


    siteName: 'GC WebTV',

    platformName: 'GC Streaming Platform',
    platformLink: 'http://globalconnect.dk',

    chooseCategory: 'Choose Category: ',
    root_category_header: 'New & Noteworthy',

    //This defines the top menu links (if empty - no custom menu links are shown)
    top_menu_links: [
        {
            url: 'http://www.dk4.dk/index.php/hjaelp-sektion/item/123',
            url_text: 'Hjælp'
        }
    ],

    settings_menu: 'Indstillinger',
    shop_menu: 'Abonnement',
    history_menu: 'Ordre historik',

    season: 'Sæson ',
    episode: 'Episode ',

    user_login_error: {
        402: 'Kontoen er deaktiveret (skal aktiveres via e-mail link)', //Account is deactivated (must activate via e-mail link)
        403: 'Kontoen er ved at blive flyttet (skal aktiveres via e-mail link)', //Account is migrated (must activate via e-mail link)
        404: 'Fejl i email eller kordeord!', //Authentication invalid (reject login)
        406: 'Denne konto er deaktiveret - kontakt GlobalConnect for information', //Account is administratively disabled
        410: 'Denne konto er slettet - kontakt GlobalConnect for information' //Account is deleted (but still recoverable | graceperiod)
    },

    password_change_msg: {
        200: {header: 'Kodeord ændret',
              body: 'Dit kodeord er nu ændret og skal bruges næste gang du logger ind',
              error_icon: false
            },
        500: {header: 'Fejl opstået',
              body: 'Dit kodeord kunne ikke ændres',
              error_icon: true
            }
    },
    save_userdata_msg: {
        200: {header: 'Instillinger gemt',
              body: 'Dine nye indstillinger er blevet gemt',
              error_icon: false
            },
        500: {header: 'Fejl opstået',
              body: 'Dine nye instillinger kunne ikke gemmes.',
              error_icon: true
            }
    },
    save_new_user_msg: {
        200: {header: 'Din konto er oprettet',
              body: 'Du vil modtage en email. Klik på linket i denne for at aktivere din nye konto',
              error_icon: false
            },
        409: {header: 'Fejl: Konto findes allerede',
              body: 'Kan ikke oprette flere bruger kontoer med samme email adresse. Hvis du har glemt dit kodeord kan du klikke på "Glemt kodeord" når du prøver at logge ind!'
            },
        500: {header: 'Fejl opstået',
              body: 'Kunne ikke oprette din bruger konto. Klik på "Hjælp" for læse mere!',
              error_icon: true
            }
    },
    generic_alert_message: {
        header: 'Der opstod en fejl',
        body: 'Desværre er der opstået en ukendt fejl - fejlkode: ' // Error code is shown hereafter
    },
    no_video_ticket: {
        header: 'Fejl opstået!',
        body: 'Videoen findes ikke eller kan ikke afspilles'
    },
    start_migration_msg: {
        header: "Vigtigt!",
        body: 'GlobalConnect har skiftet WebTV system. Dette betyder at din email adresse skal godkendes. Du vil snart modtage en email med et link som du skal klikke på. Herefter skal lave et nyt kodeord for at få adgang til GlobalConnect WebTV'
    },
    new_user_welcome: {
        header: "Velkommen",
        body: 'Du er nu registreret som bruger på GlobalConnect\'s WebTV. Du skal klikke på "Log ind" og bruge din email og kodeord for at få adgang til WebTV'
    },
    newuser: 'Opret mig',
    new_user_msg: {
        header: "Opret GlobalConnect WebTV konto",
        body: 'Dit kodeord skal være mindst 6 karakterer langt og bør indeholde både bogstaver og tal. Felter med * skal udfyldes.'
    },
    newuser_email: 'Email (til log ind)',
    close: 'Luk',
    email: 'Email',
    password: 'Kodeord',
    login: 'Log ind',
    logout: 'Log ud',
    login_msg: {
        header: "Log ind på GlobalConnect's WebTV",
        body: 'For at få adgang til GlobalConnect\'s TV arkiv skal du være oprettet som bruger. Du kan oprette dig som bruger ved at klikke på "Opret mig".'
    },
    shop_msg: {
        header: "Abonnement",
        body: 'For at få adgang til GlobalConnect\'s TV arkiv skal du have et abonnement:'
    },
    orderMessages: {
        //subscriptionOrderButton: 'Bestil ',
        //subscriptionCancelOrderButton: 'Annuller ',
        subscriptionNotActive: {          msg: '',
                                       button: 'Bestil ',
                                  buttonStyle: 'default'
                            },
        subscriptionRunning:   {          msg: 'Næste betaling sker: ',
                                       button: 'Annuller ',
                                  buttonStyle: 'cancel'
                            },
        subscriptionCanceled:   {         msg: 'Månedlig betaling er annulleret! <br> Du har adgang indtil: ',
                                       button: 'Genbestil ',
                                  buttonStyle: 'default'
                            }
    },
    user_data_msg: {
        header: "Mine indstillinger",
        body: 'Hvis du ændre dit kodeord skal det være mindst 6 karakterer langt og bør indeholde både bogstaver og tal.'
    },
    firstname: 'Fornavn',
    lastname: 'Efternavn',
    birthday: 'Fødselsdag (YYYY-MM-DD)',
    gender: 'Køn',
    male: 'Mand',
    female: 'Kvinde',
    address: 'Adresse',
    address_ext: 'Adresse (ekstra info)',
    city: 'By',
    zipcode: 'Postnummer',
    phonenumber: 'Telefonnummer',
    save_user_data: 'Gem',

    newpassword: 'Nyt kodeord',
    repeatpassword: 'Gentag kodeord',
    save_new_password: 'Skift kodeord',

    forgot_password_link: 'Glemt kodeord?',
    forgot_password_msg: {
        header: "Glemt kodeord",
        body: 'Indtast den email adresse som du har registreret hos GlobalConnect.'
    },
    forgot_password_send: 'Send',
    forgot_password_email_sent_msg: {
        header: 'Nyt kodeord',
        body: 'Du vil snart modtage en email. Klik på linket og herefter skal du indtaste et nyt kodeord.'
    },
    forgot_password_user_not_found: {
        header: 'Email findes ikke',
        body: "Den indtastede email adresse findes ikke i GlobalConnect's system!"
    },
    new_password_msg: {
        header: "Nyt kodeord",
        body: 'Dit nye kodeord skal være mindst 6 karakterer langt og bør indeholde både bogstaver og tal.'
    },
    new_password_done_msg: {
        header: "Kodeord gemt",
        body: 'Dit kodeord er nu gemt og skal brugs når du logger ind'
    }
};